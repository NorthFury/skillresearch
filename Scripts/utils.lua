-------------------------------------------------------------------------------
function RegisterEventHandlers( handlers )
    for event, handler in handlers do
        common.RegisterEventHandler( handler, event )
    end
end
-------------------------------------------------------------------------------
function RegisterReactionHandlers( handlers )
    for reaction, handler in handlers do
        common.RegisterReactionHandler( handler, reaction )
    end
end
--------------------------------------------------------------------------------
-- Helper functions
--------------------------------------------------------------------------------
function GetTableSize( t )
    if not t then
        return 0
    end
    local count = 0
    for k, v in t do
        count = count + 1
    end
    return count
end
--------------------------------------------------------------------------------
-- Logging helpers
--------------------------------------------------------------------------------
function GetStringListByArguments( argList )
    local newArgList = {}
    
    for i = 1, argList.n do
        local arg = argList[ i ]
        if common.IsWString( arg ) then
            newArgList[ i ] = arg
        else
            newArgList[ i ] = tostring( arg )
        end
    end

    return newArgList
end
--------------------------------------------------------------------------------
function LogInfo( ... )
    common.LogInfo( common.GetAddonName(), unpack( GetStringListByArguments( arg ) ) )
end
--------------------------------------------------------------------------------
function LogInfoCommon( ... )
    common.LogInfo( "common", unpack( GetStringListByArguments( arg ) ) )
end
--------------------------------------------------------------------------------
function LogWarning( ... )
    common.LogWarning( "common", unpack( GetStringListByArguments( arg ) ) )
end
--------------------------------------------------------------------------------
function LogError( ... )
    common.LogError( "common", unpack( GetStringListByArguments( arg ) ) )
end
--------------------------------------------------------------------------------
--- displays all the fields and values ??as well as the object function
function researchObj(tab,obj)

    tab = tab .. "    "

    --- limit the recursion
    if string.len (tab) > 50 then 
        LogInfo ("Recursion is limited!")
        return 
    end 

    local metaTable = getmetatable (obj)
    if metaTable then
        ---- Show the function of the object
        for k,v in pairs( metaTable ) do
            LogInfo ( tab, k,":=",v)
        end
    end
    if type(obj) == "table" then
        ---- Show the fields (variables) of the table
        if GetTableSize( obj ) == 0 
            then LogInfo ( tab, "{}")
            return
            end
        for k,v in pairs(obj) do 
            LogInfo ( tab, k,":=",v,  "{", type (v), "}")
            if type (v) == "table" and k ~= "__index"  and k ~= "_G" then
                --- "__index" - It is exactly the same table is a nested loop that leads to
                researchObj(tab,v)
            end
        end
    else
        LogInfo ( tab, "_:=",obj, "{", type (obj), "}")
    end
end
--------------------------------------------------------------------------------
function printTable(t)
    for k, v in pairs(t) do
        LogInfo(k, ', ', v);
    end
end