function registerAddonHandlers()
    local onEvent = {};

    onEvent["U_EVENT_ADDON_MEM_USAGE_REQUEST"] = function(event)
        userMods.SendEvent("U_EVENT_ADDON_MEM_USAGE_RESPONSE", {
            sender = common.GetAddonName(),
            memUsage = gcinfo()
        });
    end

    onEvent["SCRIPT_ADDON_INFO_REQUEST"] = function(event)
        userMods.SendEvent("SCRIPT_ADDON_INFO_RESPONSE", {
            sender = common.GetAddonName(),
            desc = "[r1][by Northfury] Outputs skill data in mods.txt"
        });
    end

    RegisterEventHandlers(onEvent);
end
--------------------------------------------------------------------------------
function ranksIterator(ranks)
    local i = 0;
    return function()
        local value = ranks[i];
        i = i + 1;
        if value then
            return i - 1, value;
        else
            return nil, nil;
        end
    end
end
--------------------------------------------------------------------------------
function objectToJSON(object)
    local result = '{';

    local first = true;
    for key, value in pairs(object) do
        if (not first) then
            result = result .. ','
        end
        first = false;

        result = result .. '"' .. key .. '":';
        if type(value) == "table" then
            result = result .. objectToJSON(value);
        else
            result = result .. '"' .. value .. '"';
        end
    end

    result = result .. '}';

    return result;
end
--------------------------------------------------------------------------------
function getTalentData(talentInfo)
    local talentData = {};
    talentData.ranks = {};
    --exObj2("talentInfo", talentInfo);

    if (talentInfo.parentTalentInfo) then
        local spellInfo = avatar.GetSpellInfo(talentInfo.parentTalentInfo.spellId);
        talentData.parent = {
            name = userMods.FromWString(spellInfo.name),
            x = talentInfo.parentTalentInfo.line,
            y = talentInfo.parentTalentInfo.layer
        }
    end

    for i, talent in ranksIterator(talentInfo.ranks) do
        local name = userMods.FromWString(talent.name);
        if not talentData.name then
            talentData.name = name;
        elseif talentData.name ~= name then
            talentData.name = string.gsub(name, " %a+$", "");
        end

        if not talentData.ico and talent.image then
            talentData.ico = common.GetTexturePath(talent.image);
        end

        local rankData = {};
        talentData.ranks[i] = rankData;

        if talent.spellId then
            local spellInfo = avatar.GetSpellInfo(talent.spellId);
            --exObj2("spellInfo", spellInfo);
            rankData.description = spellInfo.description;
            rankData.cooldown = spellInfo.currentValues.predictedCooldown;
            rankData.time = spellInfo.currentValues.prepareDuration;
            rankData.range = spellInfo.currentValues.range;
            rankData.mana = spellInfo.currentValues.manaCost;
            rankData.radius = spellInfo.currentValues.radius;
            if not talentData.element and spellInfo.sysSubElement then
                talentData.element = spellInfo.sysSubElement
            end
            talentData.ability = false;
        end
        if talent.abilityId then
            local abilityInfo = avatar.GetAbilityInfo(talent.abilityId);
            --exObj2("abilityInfo", abilityInfo);
            rankData.description = abilityInfo.description;
            talentData.ability = true;
        end
    end

    return talentData;
end

function getTalents()
    local data = {};

    local size = avatar.GetBaseTalentTableSize();
    for layer = 0, size.layersCount - 1, 1 do
        for line = 0, size.linesCount - 1, 1 do
            local talentInfo = avatar.GetBaseTalentInfo(layer, line);
            if talentInfo and not talentInfo.isEmpty then
                local talentData = getTalentData(talentInfo);
                talentData.z = 0;
                talentData.y = layer;
                talentData.x = line;
                table.insert(data, talentData);
            end
        end
    end

    size = avatar.GetFieldTalentTableSize();
    for field = 0, size.fieldsCount - 1, 1 do
        for row = 0, size.rowsCount - 1, 1 do
            for column = 0, size.columnsCount - 1, 1 do
                local talentInfo = avatar.GetFieldTalentInfo(field, row, column);
                if talentInfo and not talentInfo.isEmpty then
                    local talentData = getTalentData(talentInfo);
                    talentData.z = field + 1;
                    talentData.y = row;
                    talentData.x = column;
                    table.insert(data, talentData);
                end
            end
        end
    end

    return data;
end
--------------------------------------------------------------------------------
function getElementType(elementType)
    function extractElement(element)
        local valuedText = common.CreateValuedText()
        valuedText:SetFormat(userMods.ToWString("<html><t href='/Interface/Ingame/ContextRelatedTexts/SubElement/" .. element .. ".txt'/></html>"))
        local text = userMods.FromWString(common.ExtractWStringFromValuedText(valuedText))
        return text
    end

    local elements = {
        ENUM_SubElement_ACID = "Acid",
        ENUM_SubElement_ASTRAL = "Astral",
        ENUM_SubElement_COLD = "Frost",
        ENUM_SubElement_DISEASE = "Disease",
        ENUM_SubElement_FIRE = "Fire",
        ENUM_SubElement_HOLY = "Holy",
        ENUM_SubElement_LIGHTNING = "Lightning",
        ENUM_SubElement_PHYSICAL = "Physical",
        ENUM_SubElement_POISON = "Poison",
        ENUM_SubElement_SHADOW = "Shadow"
    }

    return elements[elementType];
end
function splitSentences(text)
    local delimiter = "%.%D"
    local list = {}
    local pos = 1
    if string.find("", delimiter, 1) then -- this would result in endless loops
        error("delimiter matches empty string!")
    end
    while 1 do
        local first, last = string.find(text, delimiter, pos)
        if first then -- found?
            table.insert(list, string.sub(text, pos, first))
            pos = last
        else
            table.insert(list, string.sub(text, pos))
            break
        end
    end
    return list
end

function nameToId(name)
    return string.gsub(name, " ", "_")
end

function enhanceDescription(text)
    local items = splitSentences(text);

    for i, item in items do
        if(string.find(item, '[Cc]onsumes')) then
            items[i] = '*' .. item .. '*';
        end
        if(string.find(item, '[Gg]enerates %d+ [Cc]ombat [Aa]dvantage')) then
            items[i] = '\\"' .. item .. '\\"';
        end
        items[i] = string.gsub(items[i], "Thrust", "Lunge");
        items[i] = string.gsub(items[i], '<r name="var0"/>', "X");
        items[i] = string.gsub(items[i], '<r name="var1"/>', "Y");
        items[i] = string.gsub(items[i], '<r name="var2"/>', "Z");
        items[i] = string.gsub(items[i], '<r name="var3"/>', "W");
        items[i] = string.gsub(items[i], '<html>', "");
        items[i] = string.gsub(items[i], '<html/>', "");
        items[i] = string.gsub(items[i], '<br/>', "");
        items[i] = string.gsub(items[i], '</br>', "");
    end

    return table.concat(items, '<br/>');
end

function buildLevelsItem(talent)
    local lvls = {};

    local texts = {};
    local e = {};
    local filteredE = {}
    local cleanedE = {};
    for i, rank in ranksIterator(talent.ranks) do
        e[i] = {};
        filteredE[i] = {};
        cleanedE[i] = {};
        texts[i] = userMods.FromWString(common.ExtractWStringFromValuedText(rank.description));
    end

    for i, text in ranksIterator(texts) do
        local searchPos = 0
        while (string.find(text, '[%d.]+', searchPos)) do
            local start, stop = string.find(text, '[%d.]+', searchPos);
            local value = string.sub(text, start, stop);
            table.insert(e[i], { e = value, searchPos = searchPos });
            searchPos = stop + 1;
        end
    end

    for i, x in pairs(e[0]) do
        local equal = true;
        for j, y in pairs(e) do
            if (j ~= 0 and y[i] and x.e ~= y[i].e) then
                equal = false;
            end
        end
        if (not equal) then
            for j, y in pairs(e) do
                table.insert(filteredE[j], y[i]);
            end
        end
    end

    local newText = texts[0];
    for pos = GetTableSize(filteredE[0]), 1, -1 do
        local position = filteredE[0][pos].searchPos;
        local firstPosition = position - 1;
        if (firstPosition == -1) then
            firstPosition = 0;
        end
        newText = string.sub(newText, 0, firstPosition)
                .. string.gsub(string.sub(newText, position), filteredE[0][pos].e, '!e' .. pos, 1);
        for i, x in pairs(e) do
            cleanedE[i][pos] = { e = filteredE[i][pos].e }
        end
    end

    for i, rank in ranksIterator(talent.ranks) do
        lvls[i + 1] = {
            lvl = i + 1,
            mana = rank.mana or 0,
            range = rank.mana or 0,
            time = (rank.time or 0) / 1000,
            cooldown = (rank.cooldown or 0) / 1000,
            radius = rank.radius or 0,
            damage = "",
            e = cleanedE[i]
        };
    end

    return lvls, enhanceDescription(newText);
end

function buildTalentItem(talent)
    local lvls, description = buildLevelsItem(talent);

    local body = '!element !lvl !cost !range !mana !time !cooldawn !preparation !d';
    local class = avatar.GetClass();
    if (class == "WARRIOR" or class == "PALADIN" or class == "STALKER" or class == "BARD") then
        body = '!element !lvl !cost !radius !energy !time !cooldawn !preparation !d';
    end
    if (talent.ability) then
        body = '!lvl !cost !d';
    end
    body = body .. description;

    local locks = {};
    if (talent.parent) then
        locks[0] = {
            parent = nameToId(talent.parent.name),
            side = "0"
        };
    end

    return {
        name = nameToId(talent.name),
        title = talent.name,
        element = getElementType(talent.element),
        globalEffect1 = "",
        globalEffect2 = "",
        globalEffect3 = "",
        globalEffect4 = "",
        globalEffect5 = "",
        preparation = "0",
        time = "0",
        body = body,
        ico = string.gsub(talent.ico, ".%(UITexture%).xdb", ".png"),
        icoPosition = (talent.icoPosition or 0),
        locks = locks,
        lvls = lvls,
        links = {},
        linked = {},
        modifiers = {}
    };
end

function buildMosaicItem(talent)
    return {
        x = talent.x,
        y = talent.y,
        z = talent.z,
        talent = nameToId(talent.name)
    };
end

function getNullic()
    return {
        name = "nullic",
        title = "Empty Cell",
        preparation = "0",
        element = "",
        body = "",
        globalEffect1 = "",
        globalEffect2 = "",
        globalEffect3 = "",
        ico = "",
        line = "",
        icoPosition = "0",
        time = "0",
        globalEffect4 = "",
        globalEffect5 = "",
        locks = {},
        links = {},
        linked = {},
        modifiers = {}
    };
end

function addLinksToTalents(talents)
    local names = {};
    for i, talent in pairs(talents) do
        names[talent.title] = talent;
    end

    for i, talent in pairs(talents) do
        for name, talentX in names do
            if (talent.title ~= name and string.find(talent.body, name)) then
                talent.body = string.gsub(talent.body, name, '\\"' .. name .. '\\"')
                talent.body = string.gsub(talent.body, '"\\"', '\\"');
                talent.body = string.gsub(talent.body, '\\"!"', '!\\"');
                talent.body = string.gsub(talent.body, '\\\\"', '\\"');
                local link = {
                    link = talent.name,
                    body = "",
                    func = "",
                    condition = "",
                    weight = "0",
                    is_condition = "0",
                    real_ico = "0",
                    lvl = "0"
                };
                local linked = {
                    talent = talentX.name,
                    body = "",
                    func = "",
                    condition = "",
                    weight = "0",
                    is_condition = "0",
                    real_ico = "0",
                    lvl = "0"
                }
                talentX.links[GetTableSize(talentX.links)] = link;
                talent.linked[GetTableSize(talent.linked)] = linked;
            end
        end
    end
end

function getIcons(class)
    local talentIcons = {};
    local rubyIcons = {};
    local centerRubyIcons = {};

    talentIcons['WARRIOR'] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 15, 12, 13, 14, 16, 12, 17, 2, 18, 19 };
    rubyIcons['WARRIOR'] = { 1, 2, 3, 4, 5, 6, 7, 3, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 50, 51, 52, 53, 54, 55, 56, 38, 57 };
    centerRubyIcons['WARRIOR'] = { 10, 30, 49 };

    talentIcons['STALKER'] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 };
    rubyIcons['STALKER'] = { 5, 6, 7, 8, 9, 10, 11, 54, 12, 1, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53 };
    centerRubyIcons['STALKER'] = { 2, 3, 4 };

    talentIcons['PALADIN'] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 };
    rubyIcons['PALADIN'] = { 4, 5, 6, 7, 8, 9, 11, 12, 15, 2, 10, 14, 13, 1, 16, 17, 18, 19, 20, 21, 22, 23, 24, 14, 25, 26, 27, 28, 29, 30, 31, 32, 32, 33, 34, 35, 36, 37, 38, 39, 40, 17, 41, 42, 43, 44 };
    centerRubyIcons['PALADIN'] = { 1, 2, 3 };

    talentIcons['BARD'] = { 0, 1, 2, 3, 7, 5, 6, 4, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 };
    rubyIcons['BARD'] = { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52 };
    centerRubyIcons['BARD'] = { 1, 2, 3 };

    talentIcons['PSIONIC'] = { 0, 1, 2, 3, 4, 5, 7, 6, 10, 9, 8, 13, 12, 14, 11, 15, 16, 17, 19, 18, 20, 21, 22 };
    rubyIcons['PSIONIC'] = { 5, 6, 7, 8, 9, 10, 11, 12, 13, 1, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 47, 39, 40, 25, 41, 42, 43, 44, 45, 46 };
    centerRubyIcons['PSIONIC'] = { 2, 3, 4 };

    talentIcons['PRIEST'] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 };
    rubyIcons['PRIEST'] = { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 18, 16, 17, 20, 1, 19, 20, 21, 22, 3, 23, 24, 25, 26, 27, 28, 29, 30, 39, 2, 31, 32, 33, 34, 11, 35, 36, 15, 1, 40, 3, 37, 41, 38 };
    centerRubyIcons['PRIEST'] = { 1, 2, 3 };

    talentIcons['MAGE'] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 };
    rubyIcons['MAGE'] = { 5, 17, 8, 10, 9, 11, 12, 13, 14, 15, 16, 18, 19, 20, 6, 7, 25, 22, 24, 23, 26, 27, 46, 29, 21, 28, 45, 30, 31, 32, 33, 34, 4, 1, 35, 39, 38, 37, 43, 36, 40, 42, 41, 44 };
    centerRubyIcons['MAGE'] = { 2, 3, 4 };

    talentIcons['DRUID'] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 };
    rubyIcons['DRUID'] = { 5, 6, 8, 9, 7, 7, 11, 1, 10, 13, 15, 14, 2, 18, 16, 17, 20, 19, 23, 22, 24, 3, 25, 37, 26, 29, 28, 28, 31, 30, 33, 32, 8, 4, 36, 35, 21, 15, 34, 12, 39, 40, 41, 38, 4, 43, 44, 42 };
    centerRubyIcons['DRUID'] = { 2, 3, 4 };

    talentIcons['NECROMANCER'] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 };
    rubyIcons['NECROMANCER'] = { 4, 5, 1, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 44, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 31, 33, 34, 35, 36, 37, 3, 38, 39, 40, 41, 42, 43 };
    centerRubyIcons['NECROMANCER'] = { 1, 2, 3 };

    return talentIcons[class], rubyIcons[class], centerRubyIcons[class];
end
function getClassObject(class)
    local classes = {};
    classes['WARRIOR'] = {
        name = "WARRIOR",
        title = "Warrior",
        calcUrl = "w",
        statDefault = "0",
        calcOfficer = "0",
        line = "/images/new/1.png"
    };
    classes['STALKER'] = {
        name = "STALKER",
        title = "Scout",
        calcUrl = "s",
        statDefault = "0",
        calcOfficer = "0",
        line = "/images/new/8.png"
    };
    classes['PALADIN'] = {
        name = "PALADIN",
        title = "Paladin",
        calcUrl = "t",
        statDefault = "0",
        calcOfficer = "0",
        line = "/images/new/7.png"
    };
    classes['BARD'] = {
        name = "BARD",
        title = "bard",
        calcUrl = "b",
        statDefault = "0",
        calcOfficer = "0",
        line = "/images/new/9.png"
    };
    classes['PSIONIC'] = {
        name = "PSIONIC",
        title = "Psionicist",
        calcUrl = "v",
        statDefault = "0",
        calcOfficer = "0",
        line = "/images/new/6.png"
    };
    classes['PRIEST'] = {
        name = "PRIEST",
        title = "Healer",
        calcUrl = "p",
        statDefault = "0",
        calcOfficer = "0",
        line = "/images/new/3.png"
    };
    classes['MAGE'] = {
        name = "MAGE",
        title = "Mage",
        calcUrl = "m",
        statDefault = "0",
        calcOfficer = "0",
        line = "/images/new/2.png"
    };
    classes['DRUID'] = {
        name = "DRUID",
        title = "Warden",
        calcUrl = "d",
        statDefault = "0",
        calcOfficer = "0",
        line = "/images/new/5.png"
    };
    classes['NECROMANCER'] = {
        name = "NECROMANCER",
        title = "Summoner",
        calcUrl = "n",
        statDefault = "0",
        calcOfficer = "0",
        line = "/images/new/4.png"
    };

    return classes[class];
end

function printData(data)
    local mosaic = {};
    local talents = {};
    local names = {};
    local class = avatar.GetClass();

    local talentIcons, rubyIcons, centerRubyIcons = getIcons(class);

    local rubyIcon = 1;
    local talentIcon = 1;
    for i, talent in data do
        if (not (talent.z > 0 and talent.y == 4 and talent.x == 3)) then
            table.insert(mosaic, buildMosaicItem(talent));
        end
        if (not names[talent.name]) then
            names[talent.name] = true;

            if (talent.z == 0) then
                talent.icoPosition = talentIcons[talentIcon] * 40;
                talentIcon = talentIcon + 1;
                if (talent.y == 0 and talent.x < 3) then

                    local t2 = buildTalentItem(talent);
                    t2.body = '!' .. t2.name .. '.d';
                    t2.name = 'r_' .. t2.name;
                    t2.icoPosition = centerRubyIcons[talent.x + 1] * 32;
                    table.insert(talents, t2);

                    local m2 = buildMosaicItem(talent);
                    m2.talent = 'r_' .. m2.talent;
                    m2.x = 3;
                    m2.y = 4;
                    m2.z = talent.x + 1;
                    table.insert(mosaic, m2);
                end
            else
                talent.icoPosition = rubyIcons[rubyIcon] * 32;
                rubyIcon = rubyIcon + 1;
            end

            table.insert(talents, buildTalentItem(talent));
        end
    end
    table.insert(talents, getNullic());
    addLinksToTalents(talents);

    LogInfo("--------------------");
    local class = getClassObject(class);
    local result = {
        class = class,
        talents = talents,
        mosaic = mosaic
    };

    local toPrint = objectToJSON(result);
    LogInfo(string.sub(toPrint, 0, 20000));
    LogInfo(string.sub(toPrint, 20001, 40000));
    LogInfo(string.sub(toPrint, 40001));
end
--------------------------------------------------------------------------------
-- Init
--------------------------------------------------------------------------------
function Init()
    LogInfo(avatar.GetClass());
    registerAddonHandlers();

    local data = getTalents();
    printData(data);
end
--------------------------------------------------------------------------------
Init()
